#!/usr/bin/python

from pymongo import MongoClient
from datetime import date
from random import randint

# Connexion à mongoDB
client = MongoClient('mongodb://localhost:27017')
db = client.ANR

# Création des données
names = ['Kitchen','Animal','State', 'Tastey', 'Big','City','Fish', 'Pizza','Goat', 'Salty','Sandwich','Lazy', 'Fun']
etablissement = ['Paris 13','Paris 8','Paris 4','Paris 1']
domaines = ['Informatique','Sociologie','Mathematiques','Psychologie']
author = ['Scott','Bill','Adam','Sheina']
start_dt = date.today().replace(day=1, month=1).toordinal()
end_dt = date.today().toordinal()

for x in range(0, 15):
    random_day = date.fromordinal(randint(start_dt, end_dt))
    business = {
        'Titre' : names[randint(0, (len(names)-1))] + ' ' + names[randint(0, (len(names)-1))]  + ' ' + etablissement[randint(0, (len(etablissement)-1))],
        'Etablissement' :  etablissement[randint(0, (len(etablissement)-1))],
        'Domaines' : domaines[randint(0, (len(domaines)-1))],
        'Auteurs' : author[randint(0, (len(author)-1))],
        'Date' : str(random_day),
        'Description' : "Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker."
    }
    result=db.projects.insert_one(business)

print("Vous venez de créer 15 projets universitaires.")