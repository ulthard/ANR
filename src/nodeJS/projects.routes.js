
module.exports = (app) => {
    const projects = require('./projects.controller.js');

    // Create a new Product
    app.post('/projects', projects.create);

    // Retrieve all projects
    app.get('/projects', projects.findAll);

    // Retrieve all projects with a limit
    app.get('/projects/limit/:limit', projects.findWithLimit);

    // Retrieve a single Product with projectId
    app.get('/projects/:projectId', projects.findOne);

    // Update a project with projectId
    app.put('/projects/:projectId', projects.update);

    // Delete a project with projectId
    app.delete('/projects/:projectId', projects.delete);
}