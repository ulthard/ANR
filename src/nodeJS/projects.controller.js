const Projects = require('./projects.model.js');

//Create new Product
exports.create = (req, res) => {
    // Create a Product
    const project = new Projects({
        Titre: req.body.titre || "No projects title",
        Etablissement: req.body.etablissement,
        Domaines: req.body.domaines,
        Auteurs: req.body.auteurs,
        Date: req.body.date,
        Description: req.body.description
    });

    // Save Product in the database
    project.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Something wrong while creating the project."
        });
    });
};

// Retrieve all Projects from the database.
exports.findAll = (req, res) => {
    Projects.find()
    .then(Projects => {
        res.send(Projects);
    })
    .catch(err => {
        res.status(500).send({
            message: err.message || "Something wrong while retrieving Projects."
        });
    });
};

// Retrieve all Projects from the database with a limit.
exports.findWithLimit = (req, res) => {
    limit = parseInt(req.params.limit);
    Projects.find().sort({date:-1}).limit(limit)
    .then(Projects => {
        res.send(Projects);
    })
    .catch(err => {
        res.status(500).send({
            message: err.message || "Something wrong while retrieving Projects."
        });
    });
};

// Find a single Projects with a ProjectsId
exports.findOne = (req, res) => {
    Projects.findById(req.params.projectId)
    .then(Projects => {
        if(!Projects) {
            return res.status(404).send({
                message: "Projects not found with id " + req.params.projectId
            });            
        }
        res.send(Projects);
    })
    .catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Projects not found with id " + req.params.projectId
            });                
        }
        return res.status(500).send({
            message: "Something wrong retrieving projects with id " + req.params.projectId
        });
    });
};

// Update a project
exports.update = (req, res) => {
    // Find and update project with the request body
    Projects.findByIdAndUpdate(req.params.projectId, {
        Titre: req.body.titre || "No projects title",
        Etablissement: req.body.etablissement,
        Domaines: req.body.domaines,
        Auteurs: req.body.auteurs,
        Date: req.body.date,
        Description: req.body.description
    },{new: true})
    .then(project => {
        if(!project) {
            return res.status(404).send({
                message: "Projects not found with id " + req.params.projectId
            });
        }
        res.send(project);
    })
    .catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Projects not found with id " + req.params.projectId
            });                
        }
        return res.status(500).send({
            message: "Something wrong updating projects with id " + req.params.projectId
        });
    });
};

// Delete a project with the specified project in the request
exports.delete = (req, res) => {
    Projects.findByIdAndRemove(req.params.projectId)
    .then(project => {
        if(!project) {
            return res.status(404).send({
                message: "Project not found with id " + req.params.projectId
            });
        }
        res.send({message: "Project deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Project not found with id " + req.params.projectId
            });                
        }
        return res.status(500).send({
            message: "Could not delete project with id " + req.params.projectId
        });
    });
};
