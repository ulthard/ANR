# Agence nationale de la recherche

## Description
C'est le projet web du D.U.T informatique de l'I.U.T de Villetaneuse.

## Pour commencer
Ces instructions vous permettront d'obtenir une copie du projet sur votre ordinateur.

### Prérequis
Les logiciels à installer sont :
* MongoDB
```console
foo@bar:~$ firefox https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/
```
* Pymongo
```console
foo@bar:~$ python -m pip install pymongo
```
* NodeJS
```console
foo@bar:~$ curl -sL https://deb.nodesource.com/setup_14.x | bash -
foo@bar:~$ apt-get install -y nodejs
```
* express
```console
foo@bar:~$ npm install express
```
* body-parser
```console
foo@bar:~$ npm install body-parser
```
* mongoose
```console
foo@bar:~$ npm install mongoose
```

### Installation
Une série d'exemples qui vous expliquent, étape par étape, comment faire fonctionner le projet :
* MongoDB
```console
foo@bar:~$ sudo systemctl start mongod
```
* Insérer des données :
```console
foo@bar:~$ python script.py
```
* NodeJS
```console
foo@bar:~$ node serveur.js
```
* Vérifier le serveur :
```console
foo@bar:~$ firefox http://localhost:8080/projects
```
* Le dossier angularJS doit être placé dans le répertoire /var/www/html/ :
```console
foo@bar:~$ sudo cp -R angularJS /var/www/html/
```
* Consulter le site :
```console
foo@bar:~$ firefox http://localhost/angularJS
```

## Auteurs
* **JETON Alex** - *Initial work* - [GitLab](https://gitlab.com/Spaar)

## License
Ce projet est sous licence MIT - voir le fichier [LICENSE](LICENSE) pour plus de détails.